module.exports = {
  apps : [{
    name: 'deploy-example',
    script: 'index.js',

    // Options reference: https://pm2.io/doc/en/runtime/reference/ecosystem-file/
    /*args: 'one two',
    instances: 1,*/
    autorestart: true,
    watch: false,
    max_memory_restart: '1G',
    env: {
      NODE_ENV: 'development'
    },
    env_production: {
      NODE_ENV: 'production'
    }
  }],

  deploy : {
    production : {
      user : 'gitlab-runner',
      host : '116.203.17.218',
      ref  : 'origin/master',
      repo : 'git@gitlab.com:Dainself/gitlab-ci-and-pm2-deploy',
      path : '/home/gitlab-runner/builds/r3kDnD3d/gitlab-ci-and-pm2-deploy',
      'post-deploy' : 'npm install && pm2 reload ecosystem.config.js --env production'
    }
  }
};
